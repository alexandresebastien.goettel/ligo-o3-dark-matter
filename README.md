Amp_Cal_LHO.txt and Amp_Cal_LLO.txt are csv files with the transfer function (amplitude) as a function of frequency.

The file "preliminary_epsilon10_1243393026_1243509654_H1_upper_limits.csv" contains a DataFrame - containing the columns "upper_limit" in units of PSD, "uncertainty" which represents the one-sigma std of the upper_limit, and "frequency" in Hz - such that plt.plot(df["frequency"], df["upper_limit"] will plot the upper limit as a function of frequency.

It is recommended to use "upper_limit_smooth" and "uncertainty_smooth" instead, as that data is slightly cleaned and more representative of the actual underlying distribution.
